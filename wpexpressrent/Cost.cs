﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wpexpressrent
{
    public class Cost
    {
        public float wynajem { get; set; }
        public int okres { get; set; }
        public float odbior { get; set; }
        public float zwrot { get; set; }
        public float innaLokalizacja { get; set; }
        public float wyposazenie { get; set; }
        public float opcje { get; set; }
        public float pakiet { get; set; }
        public float dodatki { get; set; }
        public float przygotowawcza { get; set; }
        public float calkowity { get; set; }
        public float depozyt { get; set; }
        public string waluta { get; set; }
    }
}
