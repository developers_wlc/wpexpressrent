﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wpexpressrent.Heplers
{
    static class DateTimeHelper
    {
        public static DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }
    }
}
