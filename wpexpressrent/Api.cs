﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;

namespace wpexpressrent
{
    static class Api
    {
#if DEBUG
        private const string apiUrl = "http://ngtest.express.pl/jsonapi/";
#else
        private const string apiUrl= "https://www.express.pl/jsonapi/";
#endif        

        private static void doGetRequest(Action<string, Action, Action<string>> callback, Action completeCallback, Action<string> failCallback, string url)
        {
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += (s, e) =>
            {
                callback(e.Error == null ? e.Result : String.Empty, completeCallback, failCallback);
            };
            wc.DownloadStringAsync(new Uri(url));
        }

        private static void doPostRequest(Action<string> callback, string url, string data) {
            WebClient wc = new WebClient();
            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";
            Debug.WriteLine(wc.Headers);
            wc.UploadStringCompleted += (s, e) =>
            {
                callback(e.Error == null ? e.Result : String.Empty);
            };
            wc.UploadStringAsync(new Uri(url), "POST", data);
        }

        public static void saveReservation(
            Action<string> callback,
            int class_id,
            string start_date,
            string end_date,
            int start_location,
            int end_location,
            string start_address,
            string end_address,
            int[] equipment,
            int [] options,
            int package,
            string name,
            string surname,
            string email,
            string phone,
            string notes)
        {

                string equipmentString = string.Empty;
                for (int i = 0; i < equipment.Length; i++)
                {
                    equipmentString += "&equipment[]=" + equipment[i];
                }

                string optionsString = string.Empty;
                for (int i = 0; i < options.Length; i++)
                {
                    optionsString += "&options[]=" + options[i];
                }

                string url = apiUrl + "saveReservation.json";
                string data = "lang=" + AppResources.api_lang +
                    "&class_id=" + class_id +
                    "&start_date=" + start_date +
                    "&end_date=" + end_date +
                    "&start_location=" + start_location +
                    "&end_location=" + end_location +
                    "&start_address=" + start_address +
                    "&end_address=" + end_address +
                    equipmentString +
                    optionsString +
                    "&package=" + package +
                    "&name=" + name +
                    "&surname=" + surname +
                    "&email=" + email +
                    "&phone=" + phone + 
                    "&notes=" + notes;

                /*if (notes != null && !notes.Equals(string.Empty)) {
                    data += "&notes=" + notes;
                }*/

                doPostRequest(callback, url, data);
        
        }

        public static void getCost(
            Action<string, Action, Action<string>> callback,
            Action completeCallback,
            int class_id,
            string start_date,
            string end_date,
            int start_location,
            int end_location,
            int[] equipment,
            int [] options,
            int package)
        {

            string equipmentString = string.Empty;
            for (int i = 0; i < equipment.Length; i++)
            {
                equipmentString += "&equipment[]=" + equipment[i];
            }

            string optionsString = string.Empty;
            for (int i = 0; i < options.Length; i++)
            {
                optionsString += "&options[]=" + options[i];
            }

            string url = apiUrl + "getCost.json?lang=" + AppResources.api_lang +
                "&class_id=" + class_id +
                "&start_date=" + start_date +
                "&end_date=" + end_date +
                "&start_location=" + start_location +
                "&end_location=" + end_location +
                equipmentString +
                optionsString +
                "&package=" + package;

            doGetRequest(callback, completeCallback, null, url);
        }

        public static void getClasses(Action<string, Action, Action<string>> callback, Action completeCallback, Action<string> failCallback, ushort cache)
        {
            string url = apiUrl + "getClasses.json?lang=" + AppResources.api_lang + "&cache=" + cache;            
            doGetRequest(callback, completeCallback, failCallback, url);
        }

        public static void getOffices(Action<string, Action, Action<string>> callback, Action completeCallback, ushort giveAll)
        {
            string url = apiUrl + "getOffices.json?lang=" + AppResources.api_lang + "&giveAll=" + giveAll;            
            doGetRequest(callback, completeCallback, null, url);
        }

        public static void getAdditons(Action<string, Action, Action<string>> callback, Action completeCallback)
        {
            string url = apiUrl + "getAdditions.json?lang=" + AppResources.api_lang;

            doGetRequest(callback, completeCallback, null, url);
        }

        public static void getAdditons(Action<string, Action, Action<string>> callback,
            Action completeCallback,
            Action<string> failCallback,
            int class_id, 
            string start_date, 
            string end_date, 
            int start_location_id, 
            int end_location_id)
        {
            string url = apiUrl + "getAdditions.json?lang=" + AppResources.api_lang +
                "&class_id=" + class_id +
                "&start_date=" + start_date +
                "&end_date=" + end_date +
                "&start_location_id=" + start_location_id +
                "&end_location_id=" + end_location_id;

            doGetRequest(callback, completeCallback, failCallback, url);
        }

        public static void getCalculatorLoc(Action<string, Action, Action<string>> callback, Action completeCallback,
            Action<string> failCallback,
            int retloc,
            int recloc,
            string retdate,
            string recdate)
        {
            string url = apiUrl + "getCalculatorLoc.json?lang=" + AppResources.api_lang +
                "&retloc=" + retloc +
                "&recloc=" + recloc +
                "&retdate=" + retdate +
                "&recdate=" + recdate;

            doGetRequest(callback, completeCallback, failCallback, url);
        }

        //deprecated
        public static void getCoords(Action<string, Action, Action<string>> callback, Action completeCallback)
        {
            string url = apiUrl + "getCoords.json";

            doGetRequest(callback, completeCallback, null, url);
        }

        //beznadziejna funkcja przypisywania - deprecated
        public static void addAdditionsDescriptionsByString(ObservableCollection<ExpressAddition> additions)
        {

            Dictionary<string, string> descriptions = new Dictionary<string, string>();
            //wersja polska
            descriptions.Add("pakiet bezpieczeństwo", AppResources.pakiet_desc_1);
            descriptions.Add("pakiet bezpieczne dziecko a", AppResources.pakiet_desc_2);
            descriptions.Add("pakiet bezpieczne dziecko b", AppResources.pakiet_desc_3);
            descriptions.Add("pakiet bezpieczne dziecko c", AppResources.pakiet_desc_4);
            descriptions.Add("pakiet bezpieczne dziecko d", AppResources.pakiet_desc_5);
            descriptions.Add("pakiet zagranica", AppResources.pakiet_desc_6);
            //wersja angielska
            descriptions.Add("safety package", AppResources.pakiet_desc_1);
            descriptions.Add("safe small child package", AppResources.pakiet_desc_2);
            descriptions.Add("safe medium child package", AppResources.pakiet_desc_3);
            //tu są dwa takie same  więc olewam 
            descriptions.Add("safe large child package", "");
            descriptions.Add("go abroad package", AppResources.pakiet_desc_6);

            foreach (var addition in additions)
            {
                if (addition.opis == null)
                {
                    string descString = "";
                    if (descriptions.TryGetValue(addition.nazwa.ToLower(), out descString))
                    {
                        addition.opis = descString;
                    }
                }
            }
        }
    }
}
