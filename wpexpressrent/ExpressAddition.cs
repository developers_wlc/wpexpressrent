﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace wpexpressrent
{
    public class ExpressAddition : INotifyPropertyChanged
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("nazwa")]
        public string nazwa { get; set; }
        [JsonProperty("jednorazowa")]
        public int jednorazowa { get; set; }
        [JsonProperty("cena")]
        public float cena { get; set; }
        [JsonProperty("ilosc")]
        public int ilosc { get; set; }
        [JsonProperty("pakiet")]
        public int pakiet { get; set; }
        [JsonProperty("typ")]
        public string typ { get; set; }
        [JsonProperty("waluta")]
        public string waluta { get; set; }
        [JsonProperty("opis")]
        public string opis { get; set; }
        [JsonProperty("promowany")]
        public int promowany { get; set; }


        private bool _isChecked = false;
        public bool IsChecked
        {
            get {
                return _isChecked;
            }
            set {
                if (value != _isChecked)
                {
                    _isChecked = value;
                    if (ShowQuantity == Visibility.Visible)
                    {
                        if (IsChecked == true)
                        {
                            if (Quantity == 0 || Quantity == -1)
                            {
                                Quantity = 1;
                            }
                        }
                        else
                        {
                            Quantity = 0;
                        }
                    }
                    NotifyPropertyChanged("IsChecked");
                }
            }
        }

        public Brush NameForeground {
            get {
                if (promowany == 1)
                    return App.Current.Resources["BrushYellow"] as SolidColorBrush;
                else
                    return App.Current.Resources["BrushGray"] as SolidColorBrush;
                    
            }
        }

        public Visibility ShowPromoted
        {
            get
            {
                if (promowany == 1)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility ShowDescription
        {
            get
            {
                if (opis != null && opis != string.Empty)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility ShowQuantity
        {
            get
            {
                if (ilosc > 1)
                {
                    return Visibility.Visible;
                }
                else {
                    return Visibility.Collapsed;
                }
            }
        }

        //private List<int> _quantityPicker = new List<int>();
        public List<int> QuantityPicker
        {
            get
            {
                List<int> _quantityPicker = new List<int>();
                if (ShowQuantity == Visibility.Visible)
                {
                    for (int i = 0; i <= ilosc; i++) {
                        _quantityPicker.Add(i);
                    }
                }
                return _quantityPicker;
            }
        }

        public string PriceTitle {
            get {
                if (jednorazowa == 0)
                {
                    return AppResources.daily;
                }
                else {
                    return AppResources.once;
                }
            }
        }
        //tu musi byc -1 bo ladowanie listy jest na callbacku 
        private int _quantity = -1;
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value != _quantity)
                {
                    _quantity = value;
                    NotifyPropertyChanged("Quantity");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
