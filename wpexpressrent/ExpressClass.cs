﻿using Newtonsoft.Json;

namespace wpexpressrent
{
    public class ExpressClass
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("nazwa")]
        public string nazwa { get; set; }
        [JsonProperty("opis")]
        public string opis { get; set; }
        [JsonProperty("obrazURL")]
        public string obrazURL { get; set; }
        [JsonProperty("obrazDuzyURL")]
        public string obrazDuzyURL { get; set; }
        [JsonProperty("cena")]
        public float cena { get; set; }
        [JsonProperty("depozyt")]
        public float depozyt { get; set; }
        [JsonProperty("waluta")]
        public string waluta { get; set; }


        public string Price1Title
        {
            get
            {
                return AppResources.daily;
            }
        }

        public string Price2Title
        {
            get
            {
                return AppResources.deposit;
            }
        }
    }
}
