﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Util.Converters
{
    public class CurrencyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (((string)value).Equals("eur"))
            {
                return "\u20AC";
            }

            if (((string)value).Equals("pln"))
            {
                return "zł";
            }
            return value;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (((string)value).Equals("\u20AC"))
            {
                return "eur";
            }

            if (((string)value).Equals("zł"))
            {
                return "pln";
            }

            return value;
        }
    }
}
