﻿using System;
using System.Windows.Data;
using wpexpressrent;

namespace Util.Converters
{
    public class DayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            
            switch (count)
            {
                case 1:
                    return AppResources.day;
                default:
                    return AppResources.days;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
