﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace wpexpressrent.UserControls
{
    public sealed partial class IconButton2 : UserControl
    {
        public IconButton2()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IconProperty =
DependencyProperty.Register("Icon", typeof(ControlTemplate), typeof(IconButton2), new PropertyMetadata(null));
        public ControlTemplate Icon
        {
            get { return (ControlTemplate)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty CaptionProperty =
DependencyProperty.Register("Caption", typeof(string), typeof(IconButton2), new PropertyMetadata(""));
        public string Caption
        {
            get { return (string)GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
DependencyProperty.Register("Description", typeof(string), typeof(IconButton2), new PropertyMetadata(""));
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
    }
}
