﻿using System.Windows;
using System.Windows.Controls;


namespace wpexpressrent.UserControls
{
    public sealed partial class IconButton1 : UserControl
    {
        public IconButton1()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IconProperty =
DependencyProperty.Register("Icon", typeof(ControlTemplate), typeof(IconButton1), new PropertyMetadata(null));
        public ControlTemplate Icon
        {
            get { return (ControlTemplate)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty CaptionProperty =
DependencyProperty.Register("Caption", typeof(string), typeof(IconButton1), new PropertyMetadata(""));
        public string Caption
        {
            get { return (string)GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }
    }
}
