﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Util.Converters;

namespace wpexpressrent.pages
{
    public partial class CalculatorResult : PhoneApplicationPage
    {
        //in
        public int retloc, recloc;
        public string retdate, recdate;

        CurrencyConverter convCurrency = new CurrencyConverter();

        public CalculatorResult()
        {
            InitializeComponent();

            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                pbDownload.Visibility = System.Windows.Visibility.Visible;
                svContent.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Api.getCalculatorLoc(
                calculateLocCallback,
                downloadComplete,
                downloadFail,
                retloc,
                recloc,
                retdate,
                recdate);
        }

        void calculateLocCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(data);
                    tbCollectionPrice.Text = jsonObject.Value<string>("koszt_odbioru");
                    tbReturnPrice.Text = jsonObject.Value<string>("koszt_zwrotu");
                    tbTransportPrice.Text = jsonObject.Value<string>("koszt_transportu");
                    string currencyString = jsonObject.Value<string>("waluta");

                    tbTransportCurrency.Text = tbReturnCurrency.Text = tbCollectionCurrency.Text = (string)convCurrency.Convert(currencyString, typeof(String), null, null);
                    if (completeCallback != null)
                        completeCallback();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych calculateLoc " + ex.Message);
                    if (failCallback != null)
                        failCallback(AppResources.error_data);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania calculateLoc");
                if (failCallback != null)
                    failCallback(AppResources.error_download);
            }
        }

        private void downloadComplete() {
            pbDownload.Visibility = System.Windows.Visibility.Collapsed;
            svContent.Visibility = System.Windows.Visibility.Visible;

        }

        private void downloadFail(string message)
        {
            pbDownload.Visibility = System.Windows.Visibility.Collapsed;
            MessageBox.Show(message);
        }
    }
}