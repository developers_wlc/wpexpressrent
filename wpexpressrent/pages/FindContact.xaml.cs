﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Tasks;

namespace wpexpressrent.pages
{
    public partial class FindContact : PhoneApplicationPage
    {
        private ExpressOffice selectedOffice;
        private SearchOfficeViewModel context = new SearchOfficeViewModel(); 

        public FindContact()
        {
            InitializeComponent();
            this.DataContext = context;
            //lbPlaces.SelectedIndex = -1;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Contact destinationPage = e.Content as Contact;
            if (destinationPage != null)
            {
                destinationPage.office = selectedOffice;
            }
            //lbPlaces.SelectedIndex = -1;
            base.OnNavigatedFrom(e);
        }


        private void updatePhrase(object sender, TextChangedEventArgs e)
        {
            context.filter(tbPhrase.Text);
        }

        private void lbPlaces_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox me = (ListBox)sender;

            if (me.SelectedIndex != -1)
            {
                selectedOffice = (ExpressOffice)me.SelectedItem;
                NavigationService.Navigate(new Uri("/Pages/Contact.xaml", UriKind.Relative));
            }
            me.SelectedIndex = -1;
        }
    }
}