﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Microsoft.Phone.Controls.Maps;
using System.Device.Location;


namespace wpexpressrent.pages
{
    public partial class ContactMap : PhoneApplicationPage
    {
        private ExpressOffice selectedOffice;

        public ContactMap()
        {
            InitializeComponent();

            //lpOffices.ItemsSource = App.ViewModel.Offices;

            mapOffices.Center = new GeoCoordinate(52.404137, 19.365091);
            foreach (ExpressOffice item in App.ViewModel.Offices)
            {
                if (item.lat != null && item.lng != null && item.lat.Length > 0 && item.lng.Length > 0)
                {
                    GeoCoordinate d = new GeoCoordinate(item.latDouble, item.lngDouble);

                    Pushpin pin;
                    pin = new Pushpin();
                    pin.Style = this.Resources["PushpinStyle"] as Style;
                    pin.Location = d;
                    pin.Tag = item;
                    pin.Tap += pin_Tap;
                    mapOffices.Children.Add(pin);
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //lpOffices.SelectedIndex = 0;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            Contact destinationPage = e.Content as Contact;
            if (destinationPage != null)
            {
                destinationPage.office = selectedOffice;
            }
        }

        void pin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Pushpin me = sender as Pushpin;
            selectedOffice = (ExpressOffice)me.Tag;
            NavigationService.Navigate(new Uri("/Pages/Contact.xaml", UriKind.Relative));            
        }



        private void mapOffices_MapZoom(object sender, MapZoomEventArgs e)
        {

        }

        private void ibtnSendEmail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "";
            emailComposeTask.Body = "";
            emailComposeTask.To = "express@express.pl";
            emailComposeTask.Show();
        }

        private void ibtnCall2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.PhoneNumber = "+48123000300";
            phoneCallTask.DisplayName = "expressrent";
            phoneCallTask.Show();
        }

        /*private void lpOffices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ind = lpOffices.SelectedIndex;
            if (ind != 0)
            {
                selectedOffice = lpOffices.SelectedItem as ExpressOffice;
                NavigationService.Navigate(new Uri("/Pages/Contact.xaml", UriKind.Relative));
            }
        }*/

        private void ibtnFindBranch_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/FindContact.xaml", UriKind.Relative));            
        }
    }
}