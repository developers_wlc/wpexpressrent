﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using wpexpressrent.Heplers;

namespace wpexpressrent.pages
{
    public partial class Calculator : PhoneApplicationPage
    {
        Style errorLpStyle = App.Current.Resources["ExpressListPickerStyleError"] as Style;
        Style okLpStyle = App.Current.Resources["ExpressListPickerStyle"] as Style;

        public Calculator()
        {
            InitializeComponent();
            DataContext = App.ViewModel;
            this.Loaded += Calculator_Loaded;
            setDefaultDates();
        }

        void Calculator_Loaded(object sender, RoutedEventArgs e)
        {
            BuildLocalizedApplicationBar();
            cbDropOff.Checked +=cbDropOff_Checked;
            cbDropOff.Unchecked += cbDropOff_Unchecked;
            
        }

        private void setDefaultDates() {
            dpPickUp.Value = DateTime.Now.AddDays(1).Date;
            dpDropOff.Value = DateTime.Now.AddDays(3).Date; ;
            tpPickUp.Value = DateTimeHelper.RoundUp(DateTime.Now.AddMinutes(30), TimeSpan.FromMinutes(15));
            tpDropOff.Value = tpPickUp.Value;
        }

        private void cbDropOff_Checked(object sender, RoutedEventArgs e)
        {
            lpDropOffs.SelectedIndex = lpPickups.SelectedIndex;
            lpDropOffs.IsEnabled = false;
            lpPickups.SelectionChanged += lpPickups_SelectionChanged;
        }

        void lpPickups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lpPickups.SelectedIndex != 0)
                validateListPickerFirstOffice(lpPickups);
        }

        private void cbDropOff_Unchecked(object sender, RoutedEventArgs e)
        {
            lpDropOffs.IsEnabled = true;
            lpPickups.SelectionChanged -= lpPickups_SelectionChanged;
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarButtonNext = new ApplicationBarIconButton(new Uri("/assets/icons/appbar/next.png", UriKind.Relative));
            appBarButtonNext.Click +=appBarButtonNext_Click;
            appBarButtonNext.Text = AppResources.next;
            ApplicationBar.Buttons.Add(appBarButtonNext);
        }

        private bool validateListPickerFirstOffice(ListPicker list)
        {
            ExpressOffice dropoff = list.SelectedItem as ExpressOffice;

            if (dropoff.isFirstSelect == true)
            {
                list.Style = errorLpStyle;
                return false;
            }
            else
            {
                list.Style = okLpStyle;
                return true;
            }
        }

        void appBarButtonNext_Click(object sender, EventArgs e)
        {

            bool result = true;

            ExpressOffice pickup = lpPickups.SelectedItem as ExpressOffice;
            ExpressOffice dropoff = lpDropOffs.SelectedItem as ExpressOffice;
            if (validateListPickerFirstOffice(lpPickups) == false)
            {
                result = false;
            }

            if (validateListPickerFirstOffice(lpDropOffs) == false)
            {
                result = false;
            }


            if(result == true)
                NavigationService.Navigate(new Uri("/Pages/CalculatorResult.xaml", UriKind.Relative));            

        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            CalculatorResult destinationPage = e.Content as CalculatorResult;
            if (destinationPage != null)
            {


                ExpressOffice pickup = lpPickups.SelectedItem as ExpressOffice;
                ExpressOffice dropoff = lpDropOffs.SelectedItem as ExpressOffice;

                DateTime? dateTimePickup = toOneTime(dpPickUp.Value.Value, tpPickUp.Value.Value);
                DateTime? dateTimeDropOff = toOneTime(dpDropOff.Value.Value, tpDropOff.Value.Value);

                string string_start_date = dateTimePickup.Value.ToString("yyyy-MM-dd") + " " + dateTimePickup.Value.ToString("HH:mm");
                string string_end_date = dateTimeDropOff.Value.ToString("yyyy-MM-dd") + " " + dateTimeDropOff.Value.ToString("HH:mm");

                // Change property of destination page
                destinationPage.recloc = pickup.id;
                destinationPage.retloc = dropoff.id;
                destinationPage.recdate = string_start_date;
                destinationPage.retdate = string_end_date;
            }
        }

        private void dpPickUp_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DatePicker me = sender as DatePicker;

            if (e.NewDateTime.Value.Date < DateTime.Now.AddDays(1).Date) {
                MessageBox.Show(AppResources.error_pickup_early_date);
                me.Value = e.OldDateTime;
            }

            if (dpDropOff.Value.Value.Date < dpPickUp.Value.Value.Date)
            {
                dpDropOff.Value = dpPickUp.Value;
            }
            
        }

        private void dpDropOff_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DatePicker me = sender as DatePicker;

            if (e.NewDateTime.Value.Date < dpPickUp.Value.Value.Date) {
                MessageBox.Show(AppResources.error_dropoff_early_date);
                dpDropOff.Value = dpPickUp.Value;    
            }
        }



        private void tpPickUp_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime pickupOneTime = toOneTime(dpPickUp.Value.Value, e.NewDateTime.Value);
            if (pickupOneTime < DateTime.Now.AddDays(1)) {
                tpPickUp.Value = DateTime.Now.AddDays(1).AddMinutes(1);
                MessageBox.Show(AppResources.error_pickup_early_time);
            }
            tpDropOff.Value = tpPickUp.Value;
        }

        private void tpDropOff_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime pickupOneTime = toOneTime(dpPickUp.Value.Value, tpPickUp.Value.Value);
            DateTime dropoffOneTime = toOneTime(dpDropOff.Value.Value, e.NewDateTime.Value);

            if (dropoffOneTime < pickupOneTime) {
                tpDropOff.Value = tpPickUp.Value.Value.AddMinutes(1);
                MessageBox.Show(AppResources.error_dropoff_early_time);
            }
        }

        private DateTime toOneTime(DateTime date, DateTime time)
        {
            DateTime oneTime = new DateTime(
                date.Year,
                date.Month,
                date.Day,
                time.Hour,
                time.Minute,
                0);
            return oneTime;
        }

        private void lpDropOffs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lpDropOffs.SelectedIndex != 0)
                validateListPickerFirstOffice(lpDropOffs);
        }

    }
}