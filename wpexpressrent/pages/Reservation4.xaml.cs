﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace wpexpressrent.pages
{
    public partial class Reservation4 : PhoneApplicationPage
    {
        public Reservation4()
        {
            InitializeComponent();
            this.DataContext = App.ReservationViewModel;


            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                pbDownload.Visibility = System.Windows.Visibility.Visible;
                svContent.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (!System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("pl-PL"))
            {
                brdInfoCard.Visibility = System.Windows.Visibility.Visible;
            }
            else {
                brdInfoCard.Visibility = System.Windows.Visibility.Collapsed;
            }
            BuildLocalizedApplicationBar();
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarButtonNext = new ApplicationBarIconButton(new Uri("/assets/icons/appbar/next.png", UriKind.Relative));
            appBarButtonNext.Click += appBarButtonNext_Click;
            appBarButtonNext.Text = AppResources.next;
            ApplicationBar.Buttons.Add(appBarButtonNext);
        }

        void appBarButtonNext_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Reservation5.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            App.ReservationViewModel.GetCost(downloadComplete);
        }

        private void downloadComplete() {
            pbDownload.Visibility = System.Windows.Visibility.Collapsed;
            svContent.Visibility = System.Windows.Visibility.Visible;

        }
    }
}