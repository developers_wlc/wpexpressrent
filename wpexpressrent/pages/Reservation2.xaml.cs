﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using wpexpressrent.Heplers;

namespace wpexpressrent.pages
{
    public partial class Reservation2 : PhoneApplicationPage
    {

        Style errorLpStyle = App.Current.Resources["ExpressListPickerStyleError"] as Style;
        Style okLpStyle = App.Current.Resources["ExpressListPickerStyle"] as Style;
        Style okTbStyle = App.Current.Resources["ExpressTextBox"] as Style;
        Style errorTbStyle = App.Current.Resources["ExpressTextBoxStyleError"] as Style;


        public Reservation2()
        {
            InitializeComponent();
            //ReservationViewModel rvm = new ReservationViewModel();
            //rvm.LoadData();
            //DataContext = rvm;
            DataContext = App.ReservationViewModel;
            this.Loaded += Reservation2_Loaded;
            setDefaultDates();
            this.dpPickUp.ValueChanged +=dpPickUp_ValueChanged;
            this.tpPickUp.ValueChanged +=tpPickUp_ValueChanged;
            this.dpDropOff.ValueChanged +=dpDropOff_ValueChanged;
            this.tpDropOff.ValueChanged +=tpDropOff_ValueChanged;
            cbDropOff.Checked += cbDropOff_Checked;
            cbDropOff.Unchecked += cbDropOff_Unchecked;
        }

        void Reservation2_Loaded(object sender, RoutedEventArgs e)
        {
            BuildLocalizedApplicationBar();
            
            
        }

        private void setDefaultDates() {
            if (App.ReservationViewModel.dateTimePickup != null)
            {
                dpPickUp.Value = App.ReservationViewModel.dateTimePickup.Value.Date;
            }
            else
            {
                dpPickUp.Value = DateTime.Now.AddDays(1).Date;
            }

            if (App.ReservationViewModel.dateTimeDropOff != null)
            {
                dpDropOff.Value = App.ReservationViewModel.dateTimeDropOff.Value.Date;
            }
            else
            {
                //dpDropOff.Value = dpPickUp.Value;
                dpDropOff.Value = DateTime.Now.AddDays(3).Date;
            }

            tpPickUp.Value = tpPickUp.Value = DateTimeHelper.RoundUp(DateTime.Now.AddMinutes(30), TimeSpan.FromMinutes(15));
            tpDropOff.Value = tpPickUp.Value;
        }

        private void cbDropOff_Checked(object sender, RoutedEventArgs e)
        {
            lpDropOffs.SelectedIndex = lpPickups.SelectedIndex;
            lpDropOffs.IsEnabled = false;
            tbAddressReturn.Text = tbAddressCollection.Text;
            //lpPickups.SelectionChanged += lpPickups_SelectionChanged;
        }

        void lpPickups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lpPickups.SelectedIndex != 0)
                validateListPickerFirstOffice(lpPickups);

            ExpressOffice pickup = lpPickups.SelectedItem as ExpressOffice;
            if (pickup.miasto == true)
            {
                spAddressCollection.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                spAddressCollection.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (cbDropOff.IsChecked == true)
            {
                lpDropOffs.SelectedIndex = lpPickups.SelectedIndex;
            }
        }

        private void cbDropOff_Unchecked(object sender, RoutedEventArgs e)
        {
            lpDropOffs.IsEnabled = true;
            //lpPickups.SelectionChanged -= lpPickups_SelectionChanged;
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarButtonNext = new ApplicationBarIconButton(new Uri("/assets/icons/appbar/next.png", UriKind.Relative));
            appBarButtonNext.Click +=appBarButtonNext_Click;
            appBarButtonNext.Text = AppResources.next;
            ApplicationBar.Buttons.Add(appBarButtonNext);
        }

        void appBarButtonNext_Click(object sender, EventArgs e)
        {
            DateTime pickDate = toOneTime(dpPickUp.Value.Value, tpPickUp.Value.Value);
            DateTime dropDate = toOneTime(dpDropOff.Value.Value, tpDropOff.Value.Value);

            if (Validate() && validatePickDrop(pickDate,dropDate))
            {
                App.ReservationViewModel.pickup = lpPickups.SelectedItem as ExpressOffice;
                App.ReservationViewModel.dropoff = lpDropOffs.SelectedItem as ExpressOffice;

                if (App.ReservationViewModel.pickup.miasto == true)
                {
                    //App.ReservationViewModel.PickupAddress = tbAddressCollection.Text;
                }
                else {
                    App.ReservationViewModel.PickupAddress = string.Empty;
                }

                if (App.ReservationViewModel.dropoff.miasto == true)
                {
                    //App.ReservationViewModel.DropOffAddress = tbAddressReturn.Text;
                }
                else
                {
                    App.ReservationViewModel.DropOffAddress = string.Empty;
                }

                App.ReservationViewModel.dateTimePickup = pickDate;
                App.ReservationViewModel.dateTimeDropOff = dropDate;

                NavigationService.Navigate(new Uri("/Pages/Reservation3.xaml", UriKind.Relative));
            }

        }

        private void dpPickUp_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            /*DatePicker me = sender as DatePicker;

            if (e.NewDateTime.Value.Date < DateTime.Now.AddDays(1).Date) {
                MessageBox.Show(AppResources.error_pickup_early_date);
                me.Value = e.OldDateTime;
            }

            if (dpDropOff.Value.Value.Date < dpPickUp.Value.Value.Date.AddDays(2))
            {
                dpDropOff.Value = dpPickUp.Value.Value.Date.AddDays(2);
            }*/

            DateTime pickupOneTime = toOneTime(e.NewDateTime.Value, tpPickUp.Value.Value);
            DateTime dropoffOneTime = toOneTime(dpDropOff.Value.Value, tpDropOff.Value.Value);

            validatePickDrop(pickupOneTime, dropoffOneTime);
            
        }

        private void dpDropOff_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            //DatePicker me = sender as DatePicker;

            /*if (e.NewDateTime.Value.Date < dpPickUp.Value.Value.Date.AddDays(2)) {
                MessageBox.Show(AppResources.error_dropoff_early_date);
                dpDropOff.Value = dpPickUp.Value.Value.AddDays(2);    
            }*/

            DateTime pickupOneTime = toOneTime(dpPickUp.Value.Value, tpPickUp.Value.Value);
            DateTime dropoffOneTime = toOneTime(e.NewDateTime.Value, tpDropOff.Value.Value);

            validatePickDrop(pickupOneTime, dropoffOneTime);
        }



        private void tpPickUp_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            /*DateTime pickupOneTime = toOneTime(dpPickUp.Value.Value, e.NewDateTime.Value);
            if (pickupOneTime < DateTime.Now.AddDays(1)) {
                tpPickUp.Value = DateTime.Now.AddDays(1).AddMinutes(1);
                MessageBox.Show(AppResources.error_pickup_early_time);
            }

            tpDropOff.Value = tpPickUp.Value;*/

            DateTime pickupOneTime = toOneTime(dpPickUp.Value.Value, e.NewDateTime.Value);
            DateTime dropoffOneTime = toOneTime(dpDropOff.Value.Value, tpDropOff.Value.Value);

            validatePickDrop(pickupOneTime, dropoffOneTime);

        }

        private void tpDropOff_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime pickupOneTime = toOneTime(dpPickUp.Value.Value, tpPickUp.Value.Value);
            DateTime dropoffOneTime = toOneTime(dpDropOff.Value.Value, e.NewDateTime.Value);

            validatePickDrop(pickupOneTime, dropoffOneTime);

            /*if (dropoffOneTime < pickupOneTime) {
                tpDropOff.Value = tpPickUp.Value.Value.AddMinutes(1);
                MessageBox.Show(AppResources.error_dropoff_early_time);
            }*/
        }

        private bool validatePickDrop(DateTime pickDate, DateTime dropDate)
        {
            if (pickDate.Date < DateTime.Now.AddDays(1).Date)
            {
                //tpPickUp.Value = DateTime.Now.AddDays(1).AddMinutes(1);
                MessageBox.Show(AppResources.error_pickup_early_date);
                return false;
            }
            else
            {
                if (pickDate < dropDate)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show(AppResources.error_dropoff_early_date);
                    return false;
                }
            }

        }

        private DateTime toOneTime(DateTime date, DateTime time)
        {
            DateTime oneTime = new DateTime(
                date.Year,
                date.Month,
                date.Day,
                time.Hour,
                time.Minute,
                0);
            return oneTime;
        }

        private bool validateListPickerFirstOffice(ListPicker list) {
            ExpressOffice dropoff = list.SelectedItem as ExpressOffice;

            if (dropoff.isFirstSelect == true)
            {
                list.Style = errorLpStyle;
                return false;
            }
            else
            {
                list.Style = okLpStyle;
                return true;
            }
        }



        private bool Validate() {

            bool result = true;

            ExpressOffice pickup = lpPickups.SelectedItem as ExpressOffice;
            ExpressOffice dropoff = lpDropOffs.SelectedItem as ExpressOffice;
            if (validateListPickerFirstOffice(lpPickups) == false) {
                result = false;
            }

            if (validateListPickerFirstOffice(lpDropOffs) == false) {
                result = false;
            }

            if (pickup.miasto == true)
            {
                if (tbAddressCollection.Text.Length <= 0)
                {
                    tbAddressCollection.Style = errorTbStyle;
                    result = false;
                }
                else
                {
                    tbAddressCollection.Style = okTbStyle;
                }
            }


            if (dropoff.miasto == true)
            {
                if (tbAddressReturn.Text.Length <= 0)
                {
                    tbAddressReturn.Style = errorTbStyle;
                    result = false;
                }
                else
                {
                    tbAddressReturn.Style = okTbStyle;
                }
            }

            return result;
        }

        private void focusValidation(object sender, RoutedEventArgs e)
        {
            Validate();
        }

        private void lpDropOffs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lpDropOffs.SelectedIndex != 0)
                validateListPickerFirstOffice(lpDropOffs);

            ExpressOffice dropoff = lpDropOffs.SelectedItem as ExpressOffice;
            if (dropoff.miasto == true)
            {
                spAddressReturn.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                spAddressReturn.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (cbDropOff.IsChecked == true) {
                tbAddressReturn.Text = tbAddressCollection.Text;
            }

        }

    }
}