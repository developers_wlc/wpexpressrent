﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;

namespace wpexpressrent.pages
{
    public partial class Reservation3 : PhoneApplicationPage
    {
        private bool canGoNext = false;

        public Reservation3()
        {
            InitializeComponent();
            DataContext = App.ReservationViewModel;
            App.ReservationViewModel.LoadAdditions(loadAdditionsSuccess, loadAdditionsFail);
            //App.ReservationViewModel.CalculateLoc(null);

            this.Loaded += Reservation3_Loaded;

            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                pbDownload.Visibility = System.Windows.Visibility.Visible;
                grdContent.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        void Reservation3_Loaded(object sender, RoutedEventArgs e)
        {
            BuildLocalizedApplicationBar();
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarButtonNext = new ApplicationBarIconButton(new Uri("/assets/icons/appbar/next.png", UriKind.Relative));
            appBarButtonNext.Click += appBarButtonNext_Click;
            appBarButtonNext.Text = AppResources.next;
            ApplicationBar.Buttons.Add(appBarButtonNext);
        }

        void appBarButtonNext_Click(object sender, EventArgs e)
        {
            if(canGoNext)
                NavigationService.Navigate(new Uri("/Pages/Reservation4.xaml", UriKind.Relative));
        }

        private void loadAdditionsSuccess()
        {
            canGoNext = true;
            pbDownload.Visibility = System.Windows.Visibility.Collapsed;
            grdContent.Visibility = System.Windows.Visibility.Visible;
        }

        private void loadAdditionsFail(string message)
        {            
            MessageBox.Show(message);
        }

        private void ListItemTemplateExtras_Checked(object sender, RoutedEventArgs e)
        {

            CheckBox checkbox = sender as CheckBox;
            ExpressAddition selectedAddition = (ExpressAddition)checkbox.DataContext;

            if (selectedAddition.pakiet == 1)
            {
                ObservableCollection<ExpressAddition> additions = App.ReservationViewModel.AdditionsUserList;
                foreach (ExpressAddition item in additions)
                {
                    if (item.pakiet == 1 && selectedAddition.id != item.id)
                    {
                        item.IsChecked = false;
                    }
                }
            }
        }

        private void ListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListPicker me = sender as ListPicker;
            if (me.SelectedItem != null) {
                ExpressAddition selectedAddition = (ExpressAddition)me.DataContext;
                if ((int)me.SelectedItem == 0)
                {
                    selectedAddition.IsChecked = false;
                }
                else {
                    selectedAddition.IsChecked = true;
                }
            }
        }

        private void spAdditionTexts_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            StackPanel me = sender as StackPanel;
            ExpressAddition selectedAddition = (ExpressAddition)me.DataContext;

            if (selectedAddition.IsChecked == true)
            {
                selectedAddition.IsChecked = false;
            }
            else {
                selectedAddition.IsChecked = true;
            }
        }
    }
}