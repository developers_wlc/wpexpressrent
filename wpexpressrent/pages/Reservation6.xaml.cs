﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Tasks;

namespace wpexpressrent.pages
{
    public partial class Reservation6 : PhoneApplicationPage
    {

        public Reservation6()
        {
            InitializeComponent();
            this.DataContext = App.ReservationViewModel;
            BuildLocalizedApplicationBar();

            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                pbDownload.Visibility = System.Windows.Visibility.Visible;
                grdStatus.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();            
            ApplicationBarIconButton appBarButtonNext = new ApplicationBarIconButton(new Uri("/assets/icons/appbar/next.png", UriKind.Relative));
            appBarButtonNext.Click += appBarButtonNext_Click;
            appBarButtonNext.Text = AppResources.next;
            ApplicationBar.Buttons.Add(appBarButtonNext);
        }

        void appBarButtonNext_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml?clearnav=1", UriKind.Relative));  
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            App.ReservationViewModel.SaveReservation(saveReservationCallback);
        }

        void saveReservationCallback(string data)
        {
            pbDownload.Visibility = System.Windows.Visibility.Collapsed;
            if (data != String.Empty)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(data);
                    int status = jsonObject.Value<int>("status");
                    switch (status)
                    {
                        case 0:
                            //success
                            tbReservationNumber.Text = jsonObject.Value<string>("numer_zewn");
                            grdStatus.Visibility = System.Windows.Visibility.Visible;
                            break;
                        default:
                            //fail
                            string apiErr = jsonObject.Value<string>("blad_loc");
                            if (apiErr == null || apiErr.Equals(string.Empty))
                            {
                                showError(AppResources.err_unknown);
                            }
                            else {
                                showError(apiErr);
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych SaveReservation " + ex.Message);
                    showError(AppResources.reservation_summary_error);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania SaveReservation");
                showError(AppResources.reservation_summary_error);
            }
        }

        private void showError(string message){
            pbDownload.Visibility = System.Windows.Visibility.Collapsed;
            tbError.Visibility = System.Windows.Visibility.Visible;
            tbError.Text = message;
        }

        private void ibtnCall2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.PhoneNumber = "+48123000300";
            phoneCallTask.DisplayName = "expressrent";
            phoneCallTask.Show();
        }
        

    }
}