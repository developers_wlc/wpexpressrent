﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace wpexpressrent.pages
{
    public partial class Reservation5 : PhoneApplicationPage
    {        
        List<TextBox> tbValidationList;

        public Reservation5()
        {
            InitializeComponent();
            makeValidationList();
            grdUserData.DataContext = App.ReservationViewModel.userData;
            this.DataContext = App.ReservationViewModel;
            BuildLocalizedApplicationBar();

#if DEBUG
            tbName.Text = "test kalicinscy";
            tbSurname.Text = "test kalicinscy";
            tbEmail.Text = "a@a.pl";
            tbPhone.Text = "123";           
#endif
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();            
            ApplicationBarIconButton appBarButtonNext = new ApplicationBarIconButton(new Uri("/assets/icons/appbar/next.png", UriKind.Relative));
            appBarButtonNext.Click += appBarButtonNext_Click;
            appBarButtonNext.Text = AppResources.next;
            ApplicationBar.Buttons.Add(appBarButtonNext);
        }

        void appBarButtonNext_Click(object sender, EventArgs e)
        {
            //to jest potrzebne bo applicationbar button nie dziala z focusem tak jak zwykly button 
            object focusObj = FocusManager.GetFocusedElement();
            if (focusObj != null && focusObj is TextBox)
            {
                var binding = (focusObj as TextBox).GetBindingExpression(TextBox.TextProperty);
                binding.UpdateSource();
            }

            if (ValidateAll())
            {
                NavigationService.Navigate(new Uri("/Pages/Reservation6.xaml", UriKind.Relative));                
            }
            else {
                System.Diagnostics.Debug.WriteLine("validation failed");
            }
            
        }

        private void makeValidationList() {
            tbValidationList = new List<TextBox>();
            tbValidationList.Add(tbName);
            tbValidationList.Add(tbSurname);
            tbValidationList.Add(tbEmail);
            tbValidationList.Add(tbPhone);
        }

        private bool ValidateAll() {
            bool result = true;

            foreach (TextBox item in tbValidationList)
            {
                if (Validate(item) == false) {
                    result = false;
                }
            }

            return result;
        }

        private bool Validate(Object sender) {
            bool result = true;

            Style okTbStyle = App.Current.Resources["ExpressTextBox"] as Style;
            Style errorTbStyle = App.Current.Resources["ExpressTextBoxStyleError"] as Style;
            Style okTbPhoneStyle = App.Current.Resources["ExpressPhoneTextBoxStyle"] as Style;
            

            if ((TextBox)sender == tbName)
                if (tbName.Text.Length <= 0)
                {
                    tbName.Style = errorTbStyle;
                    result = false;
                }
                else {
                    tbName.Style = okTbStyle;
                }

            if ((TextBox)sender == tbSurname)
                if (tbSurname.Text.Length <= 0)
                {
                    tbSurname.Style = errorTbStyle;
                    result = false;
                }
                else
                {
                    tbSurname.Style = okTbStyle;
                }

            if ((TextBox)sender == tbEmail)
                if (tbEmail.Text.Length <= 0)
                {
                    tbEmail.Style = errorTbStyle;
                    result = false;
                }
                else
                {
                    //validate email 
                    Match match = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(tbEmail.Text);
                    if (match.Success)
                    {
                        tbEmail.Style = okTbStyle;
                        tblErrorEmail.Visibility = System.Windows.Visibility.Collapsed;
                    }
                    else
                    {
                        tblErrorEmail.Visibility = System.Windows.Visibility.Visible;
                        tbEmail.Style = errorTbStyle;
                        result = false;
                    }
                }

            if ((TextBox)sender == tbPhone)
                if (tbPhone.Text.Length <= 0)
                {
                    tbPhone.Style = errorTbStyle;
                    result = false;
                }
                else
                {
                    tbPhone.Style = okTbPhoneStyle;
                }

            return result;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        private void focusValidation(object sender, RoutedEventArgs e)
        {
            Validate(sender);
        }
    }
}