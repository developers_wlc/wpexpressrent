﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;

namespace wpexpressrent
{
    public class ReservationViewModel : INotifyPropertyChanged
    {
        public ExpressClass expressClass { get; private set; }
        public ExpressOffice pickup = null;
        public ExpressOffice dropoff = null;
        public string string_start_date;
        public string string_end_date;

        //public string pikcupAddress, dropoffAddress;

        public float calc_koszt_odbioru, calc_koszt_zwrotu, calc_koszt_transportu;
        public string calc_waluta;

        public Cost Cost { get; private set; }


        /*public int package = 0;
        public int[] equipmentList;
        public int[] optionsList;*/

        public ExpressUserData userData = new ExpressUserData();

        private ObservableCollection<ExpressAddition> Additions;
        public ObservableCollection<ExpressAddition> AdditionsUserList { get; private set; }
        public ObservableCollection<ExpressOffice> Offices { get { return App.ViewModel.Offices; } }

        public ReservationViewModel() { }

        public ReservationViewModel(ExpressClass expressClass)
        {
            this.expressClass = expressClass;
        }

        private string pikcupAddress = string.Empty;
        public string PickupAddress
        {
            get { return pikcupAddress; }
            set {
                if (pikcupAddress != value) {
                    pikcupAddress = value;
                    NotifyPropertyChanged("PikcupAddress");
                }
            }
        }
        private string dropoffAddress = string.Empty;
        public string DropOffAddress
        {
            get { return dropoffAddress; }
            set
            {
                if (dropoffAddress != value)
                {
                    dropoffAddress = value;
                    NotifyPropertyChanged("DropOffAddress");
                }
            }
        }


        private DateTime? _dateTimePickup = null;
        public DateTime? dateTimePickup {
            get {
                return _dateTimePickup;
            }
            set
            {
                _dateTimePickup = value;
                string_start_date = _dateTimePickup.Value.ToString("yyyy-MM-dd") + " " + _dateTimePickup.Value.ToString("HH:mm");
            }
        }

        private DateTime? _dateTimeDropOff = null;
        public DateTime? dateTimeDropOff
        {
            get
            {
                return _dateTimeDropOff;
            }
            set
            {
                _dateTimeDropOff = value;
                string_end_date = _dateTimeDropOff.Value.ToString("yyyy-MM-dd") + " " + _dateTimeDropOff.Value.ToString("HH:mm");
            }
        }

        public string Period {
            get {
                string periodString = string.Empty;
                periodString = _dateTimePickup.Value.ToString("dd MMM yyyy") + 
                    " - " + 
                    _dateTimeDropOff.Value.ToString("dd MMM yyyy");
                return periodString;

            }
        }

        private int generatePackage(){
            //package
            int package = 0;
            foreach (ExpressAddition item in AdditionsUserList)
            {
                if (item.pakiet == 1 && item.IsChecked == true)
                {
                    package = item.id;
                    break;
                }
            }
            return package;
        }

        private int[] generateOptions() {
            //equipment
            List<int> options = new List<int>();
            foreach (ExpressAddition item in AdditionsUserList)
            {
                if (item.IsChecked == true && item.typ == "options")
                {
                    if (item.Quantity == -1 || item.Quantity == 0)
                    {
                        options.Add(item.id);
                    }
                    else
                    {
                        for (int i = 0; i < item.Quantity; i++)
                        {
                            options.Add(item.id);
                        }
                    }
                }
            }
            return options.ToArray();
        }

        private int[] generateEquipment() {
            //equipment
            List<int> equipment = new List<int>();
            foreach (ExpressAddition item in AdditionsUserList)
            {
                if (item.IsChecked == true && item.typ == "equipment")
                {
                    if (item.Quantity == -1 || item.Quantity == 0)
                    {
                        equipment.Add(item.id);
                    }
                    else
                    {
                        for (int i = 0; i < item.Quantity; i++)
                        {
                            equipment.Add(item.id);
                        }
                    }
                }
            }
            return equipment.ToArray();
        }

        public void SaveReservation(Action<string> callback)
        {



            Api.saveReservation(callback,
                expressClass.id,
                string_start_date,
                string_end_date,
                pickup.id,
                dropoff.id,
                pikcupAddress,
                dropoffAddress,
                generateEquipment(),
                generateOptions(),
                generatePackage(),
                userData.Name,
                userData.Surname,
                userData.Email,
                userData.Phone,
                userData.Notes);
        }

        public void GetCost(Action completeCallback)
        {

            Api.getCost(
                getCostCallback, 
                completeCallback,
                expressClass.id, 
                string_start_date, 
                string_end_date, 
                pickup.id, 
                dropoff.id, 
                generateEquipment(), 
                generateOptions(), 
                generatePackage());

        }

        void getCostCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    Cost = JsonConvert.DeserializeObject<Cost>(data);
                    Deployment.Current.Dispatcher.BeginInvoke(() =>{NotifyPropertyChanged("Cost");});
                    if (completeCallback != null)
                        completeCallback();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych getCost " + ex.Message);
                    if (failCallback != null)
                        failCallback(AppResources.error_data);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania getCost");
                if (failCallback != null)
                    failCallback(AppResources.error_download);
            }
        }

        public void CalculateLoc(Action completeCallback, Action<string> failCallback)
        {
            Api.getCalculatorLoc(
                calculateLocCallback, 
                completeCallback,
                failCallback,
                dropoff.id, 
                pickup.id, 
                string_end_date, 
                string_start_date);
        }

        void calculateLocCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(data);
                    calc_koszt_odbioru = jsonObject.Value<float>("koszt_odbioru");
                    calc_koszt_zwrotu = jsonObject.Value<float>("koszt_zwrotu");
                    calc_koszt_transportu = jsonObject.Value<float>("koszt_transportu");
                    calc_waluta = jsonObject.Value<string>("waluta");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych calculateLoc " + ex.Message);
                    if (failCallback != null)
                        failCallback(AppResources.error_data);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania calculateLoc");
                if (failCallback != null)
                    failCallback(AppResources.error_download);
            }
        }

        public void LoadAdditions(Action completeCallback, Action<string> failCallback)
        {
            Api.getAdditons(
                getAdditionsCallback,
                completeCallback,
                failCallback,
                expressClass.id,
                string_start_date,
                string_end_date,
                pickup.id,
                dropoff.id);
        }

        void getAdditionsCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    this.Additions = JsonConvert.DeserializeObject<ObservableCollection<ExpressAddition>>(data);
                    //tymczasowe promowanie pierwszego pakietu
                    this.Additions.Where(x => x.id == 1 && x.pakiet == 1).FirstOrDefault().promowany = 1;
                    //dodawanie opisów
                    Api.addAdditionsDescriptionsByString(this.Additions);
                    this.AdditionsUserList = new ObservableCollection<ExpressAddition>(this.Additions);
                    Deployment.Current.Dispatcher.BeginInvoke(() => { NotifyPropertyChanged("AdditionsUserList"); });
                    if (completeCallback != null)
                        completeCallback();
                }
                catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych getAdditons " + ex.Message);
                    if (failCallback != null)
                        failCallback(AppResources.error_data);
                    
                }
            }
            else {
                if (failCallback != null)
                    failCallback(AppResources.error_download);
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania getAdditons");
            }            
        }

        /*void getAdditionsCallback(IAsyncResult asynchronousResult)
        {
            WebResponse response = ((HttpWebRequest)asynchronousResult.AsyncState).EndGetResponse(asynchronousResult);
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string responseString = reader.ReadToEnd();
                this.Additions = JsonConvert.DeserializeObject<ObservableCollection<ExpressAddition>>(responseString);
            }
            response.Close();
        }*/

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
