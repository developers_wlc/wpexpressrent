﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace wpexpressrent
{
    public class ExtraViewModel : INotifyPropertyChanged
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    NotifyPropertyChanged("Description");
                }
            }
        }

        public bool IsChecked
        {
            get;
            set;
        }

        private Visibility _showQuantity = Visibility.Collapsed;
        public Visibility ShowQuantity
        {
            get
            {
                return _showQuantity;
            }
            set
            {
                if (value != _showQuantity)
                {
                    _showQuantity = value;
                    NotifyPropertyChanged("ShowQuantity");
                }
            }
        }

        private int[] _picker;
        public int[] Picker
        {
            get
            {
                return _picker;
            }
            set
            {
                if (value != _picker)
                {
                    _picker = value;
                    NotifyPropertyChanged("Picker");
                }
            }
        }

        private string _currency = "zł";
        public string Currency
        {
            get
            {
                return _currency;
            }
            set
            {
                if (value != _currency)
                {
                    _currency = value;
                    NotifyPropertyChanged("Currency");
                }
            }
        }

        private int _quantity = 1;
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value != _quantity)
                {
                    _quantity = value;
                    NotifyPropertyChanged("Quantity");
                }
            }
        }

        private int _price;
        public int Price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value != _price)
                {
                    _price = value;
                    NotifyPropertyChanged("Price");
                }
            }
        }

        private string _priceTitle = "za dobę";
        public string PriceTitle
        {
            get
            {
                return _priceTitle;
            }
            set
            {
                if (value != _priceTitle)
                {
                    _priceTitle = value;
                    NotifyPropertyChanged("PriceTitle");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}