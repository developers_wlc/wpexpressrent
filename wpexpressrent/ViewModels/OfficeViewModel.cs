﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace wpexpressrent
{
    public class OfficeViewModel : INotifyPropertyChanged
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }

        private string _nazwa;
        public string Nazwa
        {
            get
            {
                return _nazwa;
            }
            set
            {
                if (value != _nazwa)
                {
                    _nazwa = value;
                    NotifyPropertyChanged("Nazwa");
                }
            }
        }

        private string _opis;
        public string Opis
        {
            get
            {
                return _opis;
            }
            set
            {
                if (value != _opis)
                {
                    _opis = value;
                    NotifyPropertyChanged("Opis");
                }
            }
        }

        private bool _miasto;
        public bool Miasto
        {
            get
            {
                return _miasto;
            }
            set
            {
                if (value != _miasto)
                {
                    _miasto = value;
                    NotifyPropertyChanged("Miasto");
                }
            }
        }

        private bool _lotnisko;
        public bool Lotnisko
        {
            get
            {
                return _lotnisko;
            }
            set
            {
                if (value != _lotnisko)
                {
                    _lotnisko = value;
                    NotifyPropertyChanged("Lotnisko");
                }
            }
        }

        private bool _pkp;
        public bool Pkp
        {
            get
            {
                return _pkp;
            }
            set
            {
                if (value != _pkp)
                {
                    _pkp = value;
                    NotifyPropertyChanged("Pkp");
                }
            }
        }

        private string _telefon;
        public string Telefon
        {
            get
            {
                return _telefon;
            }
            set
            {
                if (value != _telefon)
                {
                    _telefon = value;
                    NotifyPropertyChanged("Telefon");
                }
            }
        }

        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (value != _email)
                {
                    _email = value;
                    NotifyPropertyChanged("Email");
                }
            }
        }

        private string _adres;
        public string Adres
        {
            get
            {
                return _adres;
            }
            set
            {
                if (value != _adres)
                {
                    _adres = value;
                    NotifyPropertyChanged("Adres");
                }
            }
        }

        private string _kodPocztowy;
        public string KodPocztowy
        {
            get
            {
                return _kodPocztowy;
            }
            set
            {
                if (value != _kodPocztowy)
                {
                    _kodPocztowy = value;
                    NotifyPropertyChanged("KodPocztowy");
                }
            }
        }

        private string _miejscowosc;
        public string Miejscowosc
        {
            get
            {
                return _miejscowosc;
            }
            set
            {
                if (value != _miejscowosc)
                {
                    _miejscowosc = value;
                    NotifyPropertyChanged("Miejscowosc");
                }
            }
        }

        private float _lat;
        public float Lat
        {
            get
            {
                return _lat;
            }
            set
            {
                if (value != _lat)
                {
                    _lat = value;
                    NotifyPropertyChanged("Lat");
                }
            }
        }

        private float _lng;
        public float Lng
        {
            get
            {
                return _lng;
            }
            set
            {
                if (value != _lng)
                {
                    _lng = value;
                    NotifyPropertyChanged("Lng");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
