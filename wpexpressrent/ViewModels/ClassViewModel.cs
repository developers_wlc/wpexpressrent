﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace wpexpressrent
{
    public class ClassViewModel : INotifyPropertyChanged
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }

        private string _nazwa;
        public string Nazwa
        {
            get
            {
                return _nazwa;
            }
            set
            {
                if (value != _nazwa)
                {
                    _nazwa = value;
                    NotifyPropertyChanged("Nazwa");
                }
            }
        }

        private string _opis;
        public string Opis
        {
            get
            {
                return _opis;
            }
            set
            {
                if (value != _opis)
                {
                    _opis = value;
                    NotifyPropertyChanged("Opis");
                }
            }
        }

        private string _obrazUrl;
        public string ObrazUrl
        {
            get
            {
                return _obrazUrl;
            }
            set
            {
                if (value != _obrazUrl)
                {
                    _obrazUrl = value;
                    NotifyPropertyChanged("ObrazUrl");
                }
            }
        }

        private string _obrazDuzyUrl;
        public string ObrazDuzyUrl
        {
            get
            {
                return _obrazDuzyUrl;
            }
            set
            {
                if (value != _obrazDuzyUrl)
                {
                    _obrazDuzyUrl = value;
                    NotifyPropertyChanged("ObrazDuzyUrl");
                }
            }
        }

        private float _cena;
        public float Cena
        {
            get
            {
                return _cena;
            }
            set
            {
                if (value != _cena)
                {
                    _cena = value;
                    NotifyPropertyChanged("Cena");
                }
            }
        }

        private float _depozyt;
        public float Depozyt
        {
            get
            {
                return _depozyt;
            }
            set
            {
                if (value != _depozyt)
                {
                    _depozyt = value;
                    NotifyPropertyChanged("Depozyt");
                }
            }
        }

        private string _waluta;
        public string Waluta
        {
            get
            {
                return _waluta;
            }
            set
            {
                if (value != _waluta)
                {
                    _waluta = value;
                    NotifyPropertyChanged("Waluta");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
