﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace wpexpressrent
{
    public class SearchOfficeViewModel : INotifyPropertyChanged
    {
        string p = "";

        public List<ExpressOffice> Offices {
            get
            {
                if (p.Length > 0)
                {
                    return App.ViewModel.OfficesClean.Where(x => x.Name.ToLower().Contains(p.ToLower())).ToList<ExpressOffice>();                    
                }
                else
                {
                    return App.ViewModel.OfficesClean.ToList<ExpressOffice>();
                }
            }
        }

        public void filter(string phrase){
            if (!p.Equals(phrase))
            {
                p = phrase;
                NotifyPropertyChanged("Offices");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
