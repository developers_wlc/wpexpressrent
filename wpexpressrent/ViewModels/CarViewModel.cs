﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace wpexpressrent
{
    public class CarViewModel : INotifyPropertyChanged
    {
        private string _class;
        public string Class
        {
            get
            {
                return _class;
            }
            set
            {
                if (value != _class)
                {
                    _class = value;
                    NotifyPropertyChanged("Class");
                }
            }
        }

        private string _names;
        public string Names
        {
            get
            {
                return _names;
            }
            set
            {
                if (value != _names)
                {
                    _names = value;
                    NotifyPropertyChanged("Names");
                }
            }
        }

        private string _currency = "zł";
        public string Currency {
            get {
                return _currency;
            }
            set {
                if (value != _currency)
                {
                    _currency = value;
                    NotifyPropertyChanged("Currency");
                }
            }
        }

        private int _price1;
        public int Price1
        {
            get
            {
                return _price1;
            }
            set
            {
                if (value != _price1)
                {
                    _price1 = value;
                    NotifyPropertyChanged("Price1");
                }
            }
        }

        private string _price1Title = "za dobę";
        public string Price1Title {
            get {
                return _price1Title;
            }
            set
            {
                if (value != _price1Title)
                {
                    _price1Title = value;
                    NotifyPropertyChanged("Price1Title");
                }
            }
        }

        private int _price2;
        public int Price2
        {
            get
            {
                return _price2;
            }
            set
            {
                if (value != _price2)
                {
                    _price2 = value;
                    NotifyPropertyChanged("Price2");
                }
            }
        }

        private string _price2Title = "depozyt";
        public string Price2Title
        {
            get
            {
                return _price2Title;
            }
            set
            {
                if (value != _price2Title)
                {
                    _price2Title = value;
                    NotifyPropertyChanged("Price2Title");
                }
            }
        }

        private string _image = "/assets/images/ph.jpg";
        public string Image {
            get { return _image; }
            set {
                if (value != _image)
                {
                    _image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}