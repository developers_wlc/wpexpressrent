﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace wpexpressrent
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<ExpressClass> Classes { get; private set; }
        public ObservableCollection<ExpressOffice> Offices { get; private set; }
        public ObservableCollection<ExpressOffice> OfficesClean { get; private set; }
        public ObservableCollection<ExpressAddition> Additions { get; private set; }        
        public ObservableCollection<NumericListItem> HowToRentSteps { get; private set; }

        System.IO.IsolatedStorage.IsolatedStorageFile local =
        System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();

        private static string CACHE_FOLDER = "ExpressCache";

        public MainViewModel()
        {

            if (!local.DirectoryExists(CACHE_FOLDER))
                local.CreateDirectory(CACHE_FOLDER);

            this.Classes = new ObservableCollection<ExpressClass>();
            this.Additions = new ObservableCollection<ExpressAddition>();

            this.HowToRentSteps = new ObservableCollection<NumericListItem>();

            this.HowToRentSteps.Add(new NumericListItem() { Id = 1, Name = AppResources.htr_1 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 2, Name = AppResources.htr_2 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 3, Name = AppResources.htr_3 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 4, Name = AppResources.htr_4 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 5, Name = AppResources.htr_5 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 6, Name = AppResources.htr_6 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 7, Name = AppResources.htr_7 });
            this.HowToRentSteps.Add(new NumericListItem() { Id = 8, Name = AppResources.htr_8 });

            IsDataLoaded = false;
        }
        
        
        private void saveFile(string name, string data){
            using (var isoFileStream = new System.IO.IsolatedStorage.IsolatedStorageFileStream(CACHE_FOLDER + "\\"+name + AppResources.api_lang+".json", System.IO.FileMode.OpenOrCreate, local))
            {
                using (var isoFileWriter = new System.IO.StreamWriter(isoFileStream))
                {
                    isoFileWriter.Write(data);
                }
            }
            App.setCacheDateNow(name);
        }

        private string readFile(string name) {
            name = name + AppResources.api_lang;
            using (var isoFileStream = new System.IO.IsolatedStorage.IsolatedStorageFileStream(CACHE_FOLDER+"\\"+name+".json", System.IO.FileMode.Open, local)){
                // Read the data.
                using (var isoFileReader = new System.IO.StreamReader(isoFileStream))
                {
                    return isoFileReader.ReadToEnd();
                }
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        public void LoadClasses(Action completeCallback, Action<string> failCallback)
        {
            string itemName = "classes";
            if (App.needRefresh(itemName))
            {
                Api.getClasses(getClassesCallback, completeCallback, failCallback, 0);
            }
            else {
                try
                {
                    this.Classes = JsonConvert.DeserializeObject<ObservableCollection<ExpressClass>>(readFile(itemName));
                    if (completeCallback != null)
                        completeCallback();
                }
                catch {
                    App.unCache(itemName);

                    if (failCallback != null)
                        failCallback(AppResources.error_data);
                }
            }
        }

        public void LoadOffices(Action completeCallback)
        {
            string itemName = "offices";
            if (App.needRefresh(itemName))
            {
                Api.getOffices(getOfficesCallback, completeCallback, 1);
            }
            else {
                try
                {
                    this.Offices = JsonConvert.DeserializeObject<ObservableCollection<ExpressOffice>>(readFile(itemName));
                    this.OfficesClean = new ObservableCollection<ExpressOffice>(this.Offices);
                    this.Offices.Insert(0, new ExpressOffice { isFirstSelect = true, nazwa = AppResources.choose });
                    if (completeCallback != null)
                        completeCallback();
                }
                catch {
                    App.unCache(itemName);
                }
                
                
            }
        }

        public void LoadAdditions(Action completeCallback)
        {
            string itemName = "additions";
            if (App.needRefresh(itemName))
            {
                Api.getAdditons(getAdditionsCallback, completeCallback);
            }
            else {
                try
                {
                    this.Additions = JsonConvert.DeserializeObject<ObservableCollection<ExpressAddition>>(readFile(itemName));
                    Api.addAdditionsDescriptionsByString(this.Additions);
                    if (completeCallback != null)
                        completeCallback();
                }
                catch {
                    App.unCache(itemName);
                }
                
            }
        }




        //deprecated
        public void LoadData(Action completeCallback)
        {

        }

        void getAdditionsCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    
                    this.Additions = JsonConvert.DeserializeObject<ObservableCollection<ExpressAddition>>(data);
                    Api.addAdditionsDescriptionsByString(this.Additions);
                    Deployment.Current.Dispatcher.BeginInvoke(() => { NotifyPropertyChanged("Additions"); });
                    saveFile("additions", data);
                    if (completeCallback != null)
                        completeCallback();
                }
                catch (Exception ex)
                {
                    if (failCallback != null)
                        failCallback(AppResources.error_data);

                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych getAdditioons " + ex.Message);
                }
            }
            else
            {
                if (failCallback != null)
                    failCallback(AppResources.error_download);
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania getAdditioons");
            }
        }

        void getClassesCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    
                    this.Classes = JsonConvert.DeserializeObject<ObservableCollection<ExpressClass>>(data);
                    Deployment.Current.Dispatcher.BeginInvoke(() =>{NotifyPropertyChanged("Classes");});
                    this.IsDataLoaded = true;
                    saveFile("classes", data);
                    if (completeCallback != null)
                        completeCallback();
                }
                catch (Exception ex)
                {
                    if (failCallback != null)
                        failCallback(AppResources.error_data);

                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych getClasses " + ex.Message);
                }
            }
            else
            {
                if (failCallback != null)
                    failCallback(AppResources.error_download);
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania getClasses");
            }
        }

        void getOfficesCallback(string data, Action completeCallback, Action<string> failCallback)
        {
            if (data != String.Empty)
            {
                try
                {
                    
                    this.Offices = JsonConvert.DeserializeObject<ObservableCollection<ExpressOffice>>(data);
                    this.OfficesClean = new ObservableCollection<ExpressOffice>(this.Offices);
                    this.Offices.Insert(0, new ExpressOffice { isFirstSelect = true, nazwa = AppResources.choose });
                    this.IsDataLoaded = true;
                    saveFile("offices", data);
                    if (completeCallback != null)
                        completeCallback();
                }
                catch (Exception ex)
                {
                    if (failCallback != null)
                        failCallback(AppResources.error_data);
                    System.Diagnostics.Debug.WriteLine("Wystąpił błąd danych getOffices " + ex.Message);
                }
            }
            else
            {
                if (failCallback != null)
                    failCallback(AppResources.error_download);
                System.Diagnostics.Debug.WriteLine("Wystąpił błąd pobierania getOffices");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}