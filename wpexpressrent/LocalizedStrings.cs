﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wpexpressrent
{
    public class LocalizedStrings
    {
        public LocalizedStrings()
        {
        }
        private static AppResources localizedResources = new AppResources();

        public AppResources LocalizedResources
        {
            get
            {
                return localizedResources;
            }
        }

    }
}
