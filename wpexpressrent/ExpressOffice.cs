﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace wpexpressrent
{
    public class ExpressOffice
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("nazwa")]
        public string nazwa { get; set; }
        [JsonProperty("opis")]
        public string opis { get; set; }
        [JsonProperty("miasto")]
        public bool miasto { get; set; }
        [JsonProperty("lotnisko")]
        public bool lotnisko { get; set; }
        [JsonProperty("pkp")]
        public bool pkp { get; set; }
        [JsonProperty("telefon")]
        public string telefon { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }
        [JsonProperty("adres")]
        public string adres { get; set; }
        [JsonProperty("kodPocztowy")]
        public string kodPocztowy { get; set; }
        [JsonProperty("miejscowosc")]
        public string miejscowosc { get; set; }

        public bool isFirstSelect = false;

        private double _doubleLat;
        public double latDouble
        {
            get { return _doubleLat; }
        }
        private string _lat;
        [JsonProperty("lat")]
        public string lat
        {
            get
            {
                return _lat;
            }
            set
            {
                _lat = value;
                _doubleLat = longStringToDouble(value);
            }
        }

        private double _doubleLng;
        public double lngDouble
        {
            get { return _doubleLng; }
        }
        private string _lng;
        [JsonProperty("lng")]
        public string lng {
            get {
                return _lng;
            }
            set {
                _lng = value;
                _doubleLng = longStringToDouble(value);
            }
        }

        public string Name {
            get { return nazwa; }
        }

        public string CodeAndCity {
            get { return this.kodPocztowy + " " + this.miejscowosc; }
        }

        public string PhoneClean
        {
            get { return this.telefon.Replace("(","").Replace(")", "").Replace(" ", ""); }
        }        

        private double longStringToDouble(string longstring)
        {
            if (longstring.Length == 0)
                return 0;

            string[] strArr = longstring.Split('.');
            strArr[1] = strArr[1].Substring(0, Math.Min(strArr[1].Length, 6));
            string strShort = strArr[0] + "." + strArr[1];
            double result = Convert.ToDouble(strShort, CultureInfo.InvariantCulture);
            return result;
        }
    }
}
