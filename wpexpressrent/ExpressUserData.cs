﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wpexpressrent
{
    public class ExpressUserData
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Notes { get; set; }
    }
}
