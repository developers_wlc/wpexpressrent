﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using wpexpressrent.UserControls;

namespace wpexpressrent
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            TiltEffect.TiltableItems.Add(typeof(IconButton1));
            TiltEffect.TiltableItems.Add(typeof(IconButton2));

            //przeniesione do app
            App.ViewModel.LoadClasses(downloadClassesComplete, downloadClassesFail);
            //App.ViewModel.LoadOffices(downloadOfficesComplete);
            //App.ViewModel.LoadAdditions(downloadAdditionsComplete);

            DataContext = App.ViewModel;

            if (!System.ComponentModel.DesignerProperties.IsInDesignTool) {
                panoramaMain.Visibility = System.Windows.Visibility.Collapsed;
                svCars.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (!System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("pl-PL"))
            {
                brdInfoConversion.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                brdInfoConversion.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void downloadClassesComplete()
        {
            System.Diagnostics.Debug.WriteLine("download classes ok");
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                svCars.Visibility = System.Windows.Visibility.Visible;
            });
        }

        private void downloadClassesFail(string message)
        {
            System.Diagnostics.Debug.WriteLine("download classes fail" + message);
            MessageBox.Show(message);
        }

        private void downloadOfficesComplete()
        {
            System.Diagnostics.Debug.WriteLine("download offices ok");
        }

        private void downloadAdditionsComplete()
        {
            System.Diagnostics.Debug.WriteLine("download additions ok");
        }

        private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PanoramaItem padded = e.AddedItems[0] as PanoramaItem;
            PanoramaItem premoved = e.RemovedItems[0] as PanoramaItem;

            Deployment.Current.Dispatcher.BeginInvoke(() => {
                padded.Style = (Style)Application.Current.Resources["StyleSelectedPanoramaItem"];
                premoved.Style = (Style)Application.Current.Resources["StylePanoramaItem"];
            });

            
        }

        private void lbCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbCars.SelectedIndex != -1)
            {
                ExpressClass selectedcar = lbCars.SelectedItem as ExpressClass;
                App.ReservationViewModel = new ReservationViewModel(selectedcar);
                NavigationService.Navigate(new Uri("/Pages/Reservation2.xaml", UriKind.Relative));
                lbCars.SelectedIndex = -1;
            }
        }

        private void ibtnFindBranch_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/ContactMap.xaml", UriKind.Relative));
        }

        private void ibtnSendEmail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "";
            emailComposeTask.Body = "";
            emailComposeTask.To = "express@express.pl";
            emailComposeTask.Show();
        }

        private void ibtnCall2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.PhoneNumber = "+48123000300";
            phoneCallTask.DisplayName = "expressrent";
            phoneCallTask.Show();
        }

        private void ibtnCheckCosts_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Calculator.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string parameter = "0";
            if (NavigationContext.QueryString.TryGetValue("clearnav", out parameter))
            {
                if (parameter == "1")
                {
                    while (this.NavigationService.BackStack.Any())
                    {

                        this.NavigationService.RemoveBackEntry();
                    }
                }

            }


            
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            panoramaMain.Visibility = System.Windows.Visibility.Visible;
        }

        private void ibtnPrivacyPolicy_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(AppResources.privacy_policy_url);
            webBrowserTask.Show(); 
        }
    }
}